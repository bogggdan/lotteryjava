package com.example.lotteryjava;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkButtonClick(View view) {
        TextView ticketField = findViewById(R.id.number_edit);
        String ticket = ticketField.getText().toString();

        if (ticket.length() < 6) {
            Toast.makeText(this, this.getText(R.string.toast_error), Toast.LENGTH_LONG).show();
        } else {
            ImageView bulb = findViewById(R.id.bulb_img);

            if (check(ticket.toCharArray())) {
                bulb.setColorFilter(this.getColor(R.color.green));
            } else {
                bulb.setColorFilter(this.getColor(R.color.red));
            }
        }
    }

    private boolean check(char[] text) {
        int res = 0;
        for (int i = 0; i < 6; i++) {
            int iterator = Character.getNumericValue(text[i]);
            res = (i >= 3 ? res - iterator : iterator + res);
        }
        return res == 0;
    }
}